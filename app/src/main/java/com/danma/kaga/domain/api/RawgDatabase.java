package com.danma.kaga.domain.api;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.danma.kaga.AppController;
import com.danma.kaga.model.CenterRepository;
import com.danma.kaga.model.entities.Product;
import com.danma.kaga.model.entities.ProductCategoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class RawgDatabase {

    private static RawgDatabase fakeServer;

    public static RawgDatabase getFakeWebServer() {

        if (null == fakeServer) {
            fakeServer = new RawgDatabase();
        }
        return fakeServer;
    }

    public void addCategory() {

        ArrayList<ProductCategoryModel> listOfCategory = new ArrayList<>();

        listOfCategory
                .add(new ProductCategoryModel(
                        "Rekomendasi Game",
                        "We give the most popular recommendations by category.",
                        "Recommendation",
                        "https://i.ibb.co/bBYD5VB/2021videogamescalendar.jpg"));

        listOfCategory
                .add(new ProductCategoryModel(
                        "All Games Platform",
                        "Menampilkan game terbaik pada masing-masing platform",
                        "Platform",
                        "https://i.ibb.co/f14PjdQ/My-Fav-Game-702x336.png\""));

        CenterRepository.getCenterRepository().setListOfCategory(listOfCategory);
    }

    public void getAllRecommendation() {

        ConcurrentHashMap<String, ArrayList<Product>> productMap = new ConcurrentHashMap<String, ArrayList<Product>>();
        final RequestQueue queue = Volley.newRequestQueue(AppController.getInstance());

        final ArrayList<Product> topRatedList = new ArrayList<Product>();
        String url ="https://api.rawg.io/api/games?key=e5460839a6d84197b33e7c47743616bc&ordering=-rating&page_size=5";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = response.getJSONArray("results");
                            for(int i=0;i<5;i++) {
                                int gameID = results.getJSONObject(i).getInt("id");
                                String url ="https://api.rawg.io/api/games/" + gameID + "?key=e5460839a6d84197b33e7c47743616bc";
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject game) {
                                                try {
                                                    int id = game.getInt("id");
                                                    String name = game.getString("name");
                                                    String description = game.getString("description_raw");
                                                    String image = game.getString("background_image");
                                                    Double rating = game.getDouble("rating");
                                                    String genre = "None";
                                                    try {
                                                        genre = game.getJSONArray("genres").getJSONObject(0).getString("name");
                                                    } catch (JSONException e) {}

                                                    topRatedList
                                                            .add(new Product(
                                                                    name,
                                                                    Double.toString(rating),
                                                                    description,
                                                                    "0",
                                                                    "0",
                                                                    genre,
                                                                    Double.toString(rating),
                                                                    image,
                                                                    Integer.toString(id)));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {}
                                });
                                queue.add(request);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {}
        });
        queue.add(request);
        productMap.put("Top Rated", topRatedList);

        final ArrayList<Product> updateList = new ArrayList<Product>();
        url ="https://api.rawg.io/api/games?key=e5460839a6d84197b33e7c47743616bc&ordering=-updated&page_size=5";
        request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = response.getJSONArray("results");
                            for(int i=0;i<5;i++) {
                                int gameID = results.getJSONObject(i).getInt("id");
                                String url ="https://api.rawg.io/api/games/" + gameID + "?key=e5460839a6d84197b33e7c47743616bc";
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject game) {
                                                try {
                                                    int id = game.getInt("id");
                                                    String name = game.getString("name");
                                                    String description = game.getString("description_raw");
                                                    String image = game.getString("background_image");
                                                    Double rating = game.getDouble("rating");
                                                    String genre = "None";
                                                    try {
                                                        genre = game.getJSONArray("genres").getJSONObject(0).getString("name");
                                                    } catch (JSONException e) {}

                                                    updateList
                                                            .add(new Product(
                                                                    name,
                                                                    Double.toString(rating),
                                                                    description,
                                                                    "0",
                                                                    "0",
                                                                    genre,
                                                                    Double.toString(rating),
                                                                    image,
                                                                    Integer.toString(id)));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {}
                                });
                                queue.add(request);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {}
        });
        queue.add(request);
        productMap.put("Latest Update", updateList);

        final ArrayList<Product> releaseList = new ArrayList<Product>();
        url ="https://api.rawg.io/api/games?key=e5460839a6d84197b33e7c47743616bc&ordering=-released&page_size=5";
        request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = response.getJSONArray("results");
                            for(int i=0;i<5;i++) {
                                int gameID = results.getJSONObject(i).getInt("id");
                                String url ="https://api.rawg.io/api/games/" + gameID + "?key=e5460839a6d84197b33e7c47743616bc";
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject game) {
                                                try {
                                                    int id = game.getInt("id");
                                                    String name = game.getString("name");
                                                    String description = game.getString("description_raw");
                                                    String image = game.getString("background_image");
                                                    Double rating = game.getDouble("rating");
                                                    String genre = "None";
                                                    try {
                                                        genre = game.getJSONArray("genres").getJSONObject(0).getString("name");
                                                    } catch (JSONException e) {}

                                                    releaseList
                                                            .add(new Product(
                                                                    name,
                                                                    Double.toString(rating),
                                                                    description,
                                                                    "0",
                                                                    "0",
                                                                    genre,
                                                                    Double.toString(rating),
                                                                    image,
                                                                    Integer.toString(id)));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {}
                                });
                                queue.add(request);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {}
        });
        queue.add(request);
        productMap.put("Newest Release", releaseList);

        ArrayList<Product> oldList = new ArrayList<Product>();
        oldList
                .add(new Product(
                        "Tennis for Two",
                        "0.0",
                        "<p>Sports video game which simulates a game of tennis. Tennis for Two was one of the first games developed in the early history of video games.</p>",
                        "0",
                        "0",
                        "Sports",
                        "0.0",
                        "https://media.rawg.io/media/screenshots/4dd/4dd09ec660cdc02e28dd4333d1cacde5.jpg",
                        "541258"));
        oldList
                .add(new Product(
                        "Spacewar!",
                        "4.0",
                        "Spacewar! is a space combat video game developed in 1962 by Steve Russell, in collaboration with Martin Graetz and Wayne Wiitanen, and programmed by Russell with assistance from others including Bob Saunders and Steve Piner. It was written for the newly installed DEC PDP-1 at the Massachusetts Institute of Technology. After its initial creation, Spacewar was expanded further by other students and employees of universities in the area, including Dan Edwards and Peter Samson. It was also spread to many of the few dozen, primarily academic, installations of the PDP-1 computer, making Spacewar the first known video game to be played at multiple computer installations.",
                        "0",
                        "0",
                        "None",
                        "4.0",
                        "https://media.rawg.io/media/screenshots/990/990b709877979478566582b4084ab599.jpg",
                        "58172"));
        oldList
                .add(new Product(
                        "Empire",
                        "0.0",
                        "<p>Empire (or Classic Empire) is a turn-based wargame with simple rules. The game was conceived by Walter Bright starting in 1971, based on various war movies and board games, notably Battle of Britain and Risk. The first version on a computer was released in 1977, and ported to many platforms in the 1970s and 80s. Several commercial versions were also released, often adding basic graphics to the originally text-based user interface. The basic gameplay is strongly reminiscent of several later games, notably Civilization.</p>",
                        "0",
                        "0",
                        "Strategy",
                        "0.0",
                        "https://media.rawg.io/media/screenshots/6af/6af9893f70c2da5f0fa5b8bcdce7b27c.jpg",
                        "55012"));
        oldList
                .add(new Product(
                        "Star Trek (1971)",
                        "0.0",
                        "<p>Star Trek is a text-based strategy video game based on the Star Trek television series and originally developed by Mike Mayfield in 1971. In the game, the player commands the USS Enterprise on a mission to hunt down and destroy an invading fleet of Klingon warships. The player travels through the 64 quadrants of the galaxy to attack enemy ships with phasers and photon torpedoes in turn-based battles and refuel at starbases in order to eliminate all enemies before running out of time.</p>\\n<p>Mayfield originally wrote the game in the BASIC programming language for the SDS Sigma 7 mainframe computer with the goal of creating a game like Spacewar! (1962) that could be played with a teleprinter instead of a graphical display. He then rewrote it for the HP 2000C minicomputer in 1972, and it was included in Hewlett-Packard&#39;s public domain software catalog the following year. It was picked up from there by David H. Ahl, who ported it with Mary Cole to BASIC-PLUS and published the source code in the Digital Equipment Corporation Edu newsletter. It was republished with other computer games in his best-selling 101 BASIC Computer Games book. Bob Leedom then expanded the game in 1974 into Super Star Trek, which was later printed by Ahl in BASIC Computer Games, the first million-selling computer book.</p>\\n<p>Star Trek, especially the Super Star Trek version, was immensely popular during the early microcomputer era. This, along with the availability of the source code, led to numerous ports of both versions of the game. Additionally, dozens of variants and expansions were made for a variety of systems. Ahl claimed in 1978 that it was difficult to find a computer installation that did not contain a version of Star Trek, and by 1980, Star Trek was described by The Dragon magazine as &quot;one of the most popular (if not the most popular) computer games around&quot;, with &quot;literally scores of different versions of this game floating around&quot;.</p>",
                        "0",
                        "0",
                        "Strategy",
                        "0.0",
                        "https://media.rawg.io/media/screenshots/d70/d70332c3f3db6044bd7c49fdb5608f7e.jpg",
                        "425207"));
        oldList
                .add(new Product(
                        "The Oregon Trail (1971)",
                        "3.6",
                        "<p>The Oregon Trail is a computer game originally developed by Don Rawitsch, Bill Heinemann, and Paul Dillenberger in 1971 and produced by the Minnesota Educational Computing Consortium (MECC) in 1974. The original game was designed to teach school children about the realities of 19th-century pioneer life on the Oregon Trail. The player assumes the role of a wagon leader guiding a party of settlers from Independence, Missouri, to Oregon's Willamette Valley via a covered wagon in 1848.</p>\\n<p>The game is the first entry in the Oregon Trail series, and has since been released in many editions by various developers and publishers who have acquired rights to it, as well as inspiring a number of spinoffs (such as The Yukon Trail and The Amazon Trail) and the parody/homage The Organ Trail.</p>",
                        "0",
                        "0",
                        "Adventure",
                        "3.6",
                        "https://media.rawg.io/media/screenshots/838/838b67befcf4002999fbcb4058479f78.jpg",
                        "56681"));
        productMap.put("Oldest Games", oldList);

        CenterRepository.getCenterRepository().setMapOfProductsInCategory(productMap);

    }

    public void getAllPlatform() {

        ConcurrentHashMap<String, ArrayList<Product>> productMap = new ConcurrentHashMap<String, ArrayList<Product>>();
        final RequestQueue queue = Volley.newRequestQueue(AppController.getInstance());

        final ArrayList<Product> pcList = new ArrayList<Product>();
        String url ="https://api.rawg.io/api/games?key=e5460839a6d84197b33e7c47743616bc&parent_platforms=1&ordering=-rating&page_size=5";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = response.getJSONArray("results");
                            for(int i=0;i<5;i++) {
                                int gameID = results.getJSONObject(i).getInt("id");
                                String url ="https://api.rawg.io/api/games/" + gameID + "?key=e5460839a6d84197b33e7c47743616bc";
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject game) {
                                                try {
                                                    int id = game.getInt("id");
                                                    String name = game.getString("name");
                                                    String description = game.getString("description_raw");
                                                    String image = game.getString("background_image");
                                                    Double rating = game.getDouble("rating");
                                                    String genre = "None";
                                                    try {
                                                        genre = game.getJSONArray("genres").getJSONObject(0).getString("name");
                                                    } catch (JSONException e) {}

                                                    pcList
                                                            .add(new Product(
                                                                    name,
                                                                    Double.toString(rating),
                                                                    description,
                                                                    "0",
                                                                    "0",
                                                                    genre,
                                                                    Double.toString(rating),
                                                                    image,
                                                                    Integer.toString(id)));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {}
                                });
                                queue.add(request);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {}
        });
        queue.add(request);
        productMap.put("PC", pcList);

        final ArrayList<Product> psList = new ArrayList<Product>();
        url ="https://api.rawg.io/api/games?key=e5460839a6d84197b33e7c47743616bc&parent_platforms=2&ordering=-rating&page_size=5";
        request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = response.getJSONArray("results");
                            for(int i=0;i<5;i++) {
                                int gameID = results.getJSONObject(i).getInt("id");
                                String url ="https://api.rawg.io/api/games/" + gameID + "?key=e5460839a6d84197b33e7c47743616bc";
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject game) {
                                                try {
                                                    int id = game.getInt("id");
                                                    String name = game.getString("name");
                                                    String description = game.getString("description_raw");
                                                    String image = game.getString("background_image");
                                                    Double rating = game.getDouble("rating");
                                                    String genre = "None";
                                                    try {
                                                        genre = game.getJSONArray("genres").getJSONObject(0).getString("name");
                                                    } catch (JSONException e) {}

                                                    psList
                                                            .add(new Product(
                                                                    name,
                                                                    Double.toString(rating),
                                                                    description,
                                                                    "0",
                                                                    "0",
                                                                    genre,
                                                                    Double.toString(rating),
                                                                    image,
                                                                    Integer.toString(id)));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {}
                                });
                                queue.add(request);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {}
        });
        queue.add(request);
        productMap.put("PlayStation", psList);

        final ArrayList<Product> xboxList = new ArrayList<Product>();
        url ="https://api.rawg.io/api/games?key=e5460839a6d84197b33e7c47743616bc&parent_platforms=3&ordering=-rating&page_size=5";
        request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = response.getJSONArray("results");
                            for(int i=0;i<5;i++) {
                                int gameID = results.getJSONObject(i).getInt("id");
                                String url ="https://api.rawg.io/api/games/" + gameID + "?key=e5460839a6d84197b33e7c47743616bc";
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject game) {
                                                try {
                                                    int id = game.getInt("id");
                                                    String name = game.getString("name");
                                                    String description = game.getString("description_raw");
                                                    String image = game.getString("background_image");
                                                    Double rating = game.getDouble("rating");
                                                    String genre = "None";
                                                    try {
                                                        genre = game.getJSONArray("genres").getJSONObject(0).getString("name");
                                                    } catch (JSONException e) {}

                                                    xboxList
                                                            .add(new Product(
                                                                    name,
                                                                    Double.toString(rating),
                                                                    description,
                                                                    "0",
                                                                    "0",
                                                                    genre,
                                                                    Double.toString(rating),
                                                                    image,
                                                                    Integer.toString(id)));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {}
                                });
                                queue.add(request);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {}
        });
        queue.add(request);
        productMap.put("XBox", xboxList);

        final ArrayList<Product> nintendoList = new ArrayList<Product>();
        url ="https://api.rawg.io/api/games?key=e5460839a6d84197b33e7c47743616bc&parent_platforms=7&ordering=-rating&page_size=5";
        request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = response.getJSONArray("results");
                            for(int i=0;i<5;i++) {
                                int gameID = results.getJSONObject(i).getInt("id");
                                String url ="https://api.rawg.io/api/games/" + gameID + "?key=e5460839a6d84197b33e7c47743616bc";
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject game) {
                                                try {
                                                    int id = game.getInt("id");
                                                    String name = game.getString("name");
                                                    String description = game.getString("description_raw");
                                                    String image = game.getString("background_image");
                                                    Double rating = game.getDouble("rating");
                                                    String genre = "None";
                                                    try {
                                                        genre = game.getJSONArray("genres").getJSONObject(0).getString("name");
                                                    } catch (JSONException e) {}

                                                    nintendoList
                                                            .add(new Product(
                                                                    name,
                                                                    Double.toString(rating),
                                                                    description,
                                                                    "0",
                                                                    "0",
                                                                    genre,
                                                                    Double.toString(rating),
                                                                    image,
                                                                    Integer.toString(id)));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {}
                                });
                                queue.add(request);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {}
        });
        queue.add(request);
        productMap.put("Nintendo", nintendoList);

        final ArrayList<Product> androidList = new ArrayList<Product>();
        url ="https://api.rawg.io/api/games?key=e5460839a6d84197b33e7c47743616bc&parent_platforms=8&ordering=-rating&page_size=5";
        request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = response.getJSONArray("results");
                            for(int i=0;i<5;i++) {
                                int gameID = results.getJSONObject(i).getInt("id");
                                String url ="https://api.rawg.io/api/games/" + gameID + "?key=e5460839a6d84197b33e7c47743616bc";
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject game) {
                                                try {
                                                    int id = game.getInt("id");
                                                    String name = game.getString("name");
                                                    String description = game.getString("description_raw");
                                                    String image = game.getString("background_image");
                                                    Double rating = game.getDouble("rating");
                                                    String genre = "None";
                                                    try {
                                                        genre = game.getJSONArray("genres").getJSONObject(0).getString("name");
                                                    } catch (JSONException e) {}

                                                    androidList
                                                            .add(new Product(
                                                                    name,
                                                                    Double.toString(rating),
                                                                    description,
                                                                    "0",
                                                                    "0",
                                                                    genre,
                                                                    Double.toString(rating),
                                                                    image,
                                                                    Integer.toString(id)));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {}
                                });
                                queue.add(request);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {}
        });
        queue.add(request);
        productMap.put("Android", androidList);

        final ArrayList<Product> iosList = new ArrayList<Product>();
        url ="https://api.rawg.io/api/games?key=e5460839a6d84197b33e7c47743616bc&parent_platforms=4&ordering=-rating&page_size=5";
        request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = response.getJSONArray("results");
                            for(int i=0;i<5;i++) {
                                int gameID = results.getJSONObject(i).getInt("id");
                                String url ="https://api.rawg.io/api/games/" + gameID + "?key=e5460839a6d84197b33e7c47743616bc";
                                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject game) {
                                                try {
                                                    int id = game.getInt("id");
                                                    String name = game.getString("name");
                                                    String description = game.getString("description_raw");
                                                    String image = game.getString("background_image");
                                                    Double rating = game.getDouble("rating");
                                                    String genre = "None";
                                                    try {
                                                        genre = game.getJSONArray("genres").getJSONObject(0).getString("name");
                                                    } catch (JSONException e) {}

                                                    iosList
                                                            .add(new Product(
                                                                    name,
                                                                    Double.toString(rating),
                                                                    description,
                                                                    "0",
                                                                    "0",
                                                                    genre,
                                                                    Double.toString(rating),
                                                                    image,
                                                                    Integer.toString(id)));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {}
                                });
                                queue.add(request);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {}
        });
        queue.add(request);
        productMap.put("iOS", iosList);

        CenterRepository.getCenterRepository().setMapOfProductsInCategory(productMap);

    }

    public void getAllProducts(int productCategory) {

        if (productCategory == 0) {

            getAllRecommendation();

        }
        else {

            getAllPlatform();

        }

    }

}
