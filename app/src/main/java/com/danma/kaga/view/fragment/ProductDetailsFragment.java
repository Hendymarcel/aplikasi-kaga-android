package com.danma.kaga.view.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.danma.kaga.R;
import com.danma.kaga.model.CenterRepository;
import com.danma.kaga.util.ColorGenerator;
import com.danma.kaga.util.Utils;
import com.danma.kaga.util.Utils.AnimationType;
import com.danma.kaga.view.activities.ECartHomeActivity;
import com.danma.kaga.view.adapter.SimilarProductsPagerAdapter;
import com.danma.kaga.view.customview.ClickableViewPager;
import com.danma.kaga.view.customview.ClickableViewPager.OnItemClickListener;
import com.danma.kaga.view.customview.LabelView;
import com.danma.kaga.view.customview.TextDrawable;
import com.danma.kaga.view.customview.TextDrawable.IBuilder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class ProductDetailsFragment extends Fragment {

    private int productListNumber;
    private ImageView itemImage;
    private TextView itemSellPrice;
    private TextView itemName;
    private TextView quanitity;
    private TextView itemdescription;
    private String itemId = "";
    private IBuilder mDrawableBuilder;
    private TextDrawable drawable;
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private String subcategoryKey;
    private boolean isFromCart;
    private ClickableViewPager similarProductsPager;
    private Toolbar mToolbar;

    /**
     * Instantiates a new product details fragment.
     */
    public ProductDetailsFragment(String subcategoryKey, int productNumber,
                                  boolean isFromCart) {

        this.subcategoryKey = subcategoryKey;
        this.productListNumber = productNumber;
        this.isFromCart = isFromCart;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_product_detail,
                container, false);

        mToolbar = (Toolbar) rootView.findViewById(R.id.htab_toolbar);
        if (mToolbar != null) {
            ((ECartHomeActivity) getActivity()).setSupportActionBar(mToolbar);
        }

        if (mToolbar != null) {
            ((ECartHomeActivity) getActivity()).getSupportActionBar()
                    .setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationIcon(R.drawable.ic_drawer);

        }

        mToolbar.setTitleTextColor(Color.WHITE);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ECartHomeActivity) getActivity()).getmDrawerLayout()
                        .openDrawer(GravityCompat.START);
            }
        });

        ((ECartHomeActivity) getActivity()).getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);

        similarProductsPager = (ClickableViewPager) rootView
                .findViewById(R.id.similar_products_pager);

        itemSellPrice = ((TextView) rootView
                .findViewById(R.id.category_discount));

        quanitity = ((TextView) rootView.findViewById(R.id.iteam_amount));

        itemName = ((TextView) rootView.findViewById(R.id.product_name));

        itemdescription = ((TextView) rootView
                .findViewById(R.id.product_description));

        itemImage = (ImageView) rootView.findViewById(R.id.product_image);

        fillProductData();

        rootView.findViewById(R.id.add_item).setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                    }

                });

        rootView.findViewById(R.id.remove_item).setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                    }

                });

        rootView.findViewById(R.id.share_game).setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        String namaGame = itemName.getText().toString();
                        String genreGame = itemSellPrice.getText().toString();
                        String ratingGame = quanitity.getText().toString();
                        String idGame = itemId;
                        String teksShare = "Check out this game!" +
                                "\nName   : " + namaGame +
                                "\nGenre  : " + genreGame +
                                "\nRating : " + ratingGame +
                                "\nLink : rawg.io/games/" + idGame;

                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, teksShare);
                        sendIntent.setType("text/plain");

                        Intent shareIntent = Intent.createChooser(sendIntent, null);
                        startActivity(shareIntent);
                    }

                });

        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP
                        && keyCode == KeyEvent.KEYCODE_BACK) {

                    Utils.switchContent(R.id.frag_container,
                            Utils.PRODUCT_OVERVIEW_FRAGMENT_TAG,
                            ((ECartHomeActivity) (getActivity())),
                            AnimationType.SLIDE_RIGHT);

                }
                return true;
            }
        });

        showRecomondation();

        return rootView;
    }

    private void showRecomondation() {

        SimilarProductsPagerAdapter mCustomPagerAdapter = new SimilarProductsPagerAdapter(
                getActivity(), subcategoryKey);

        similarProductsPager.setAdapter(mCustomPagerAdapter);

        similarProductsPager.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(int position) {

                productListNumber = position;

                fillProductData();

                Utils.vibrate(getActivity());

            }
        });
    }

    public void fillProductData() {

        //Fetch and display item from Gloabl Data Model

        itemId = CenterRepository.getCenterRepository()
                .getMapOfProductsInCategory().get(subcategoryKey).get(productListNumber)
                .getProductId();

        itemName.setText(CenterRepository.getCenterRepository()
                .getMapOfProductsInCategory().get(subcategoryKey).get(productListNumber)
                .getItemName());

        quanitity.setText(CenterRepository.getCenterRepository()
                .getMapOfProductsInCategory().get(subcategoryKey).get(productListNumber)
                .getQuantity());

        itemdescription.setText(CenterRepository.getCenterRepository()
                .getMapOfProductsInCategory().get(subcategoryKey).get(productListNumber)
                .getItemDetail());

        String sellCostString = CenterRepository
                .getCenterRepository().getMapOfProductsInCategory()
                .get(subcategoryKey).get(productListNumber)
                .getSellMRP();

        String buyMRP = "";

        String costString = sellCostString + buyMRP;

        itemSellPrice.setText(costString, BufferType.SPANNABLE);

        Spannable spannable = (Spannable) itemSellPrice.getText();

        spannable.setSpan(new StrikethroughSpan(), sellCostString.length(),
                costString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mDrawableBuilder = TextDrawable.builder().beginConfig()
                .withBorder(4).endConfig().roundRect(10);

        drawable = mDrawableBuilder.build(
                String.valueOf(CenterRepository.getCenterRepository()
                        .getMapOfProductsInCategory().get(subcategoryKey)
                        .get(productListNumber).getItemName().charAt(0)),
                mColorGenerator.getColor(CenterRepository
                        .getCenterRepository().getMapOfProductsInCategory()
                        .get(subcategoryKey).get(productListNumber)
                        .getItemName()));

        Picasso.with(getActivity())
                .load(CenterRepository.getCenterRepository().getMapOfProductsInCategory()
                        .get(subcategoryKey).get(productListNumber)
                        .getImageURL()).placeholder(drawable)
                .error(drawable).fit().centerCrop()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(itemImage, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        // Try again online if cache failed

                        Picasso.with(getActivity())
                                .load(CenterRepository.getCenterRepository()
                                        .getMapOfProductsInCategory()
                                        .get(subcategoryKey)
                                        .get(productListNumber)
                                        .getImageURL())
                                .placeholder(drawable).error(drawable)
                                .fit().centerCrop().into(itemImage);
                    }
                });

        LabelView label = new LabelView(getActivity());

        label.setText(CenterRepository.getCenterRepository().getMapOfProductsInCategory()
                .get(subcategoryKey).get(productListNumber).getDiscount());
        label.setBackgroundColor(0xffE91E63);

        label.setTargetView(itemImage, 10, LabelView.Gravity.RIGHT_TOP);
    }

}
