package com.danma.kaga.model.entities;

/**
 * @author Hitesh
 */
public class ProductCategoryModel {

    private String categoryName;
    private String categoryDescription;
    private String categoryDiscount;
    private String categoryImageUrl;
    /**
     * @param productCategoryName
     * @param productCategoryDescription
     * @param productCategoryDiscount
     * @param productCategoryUrl
     */
    public ProductCategoryModel(String productCategoryName, String productCategoryDescription,
                                String productCategoryDiscount, String productCategoryUrl) {
        super();
        this.categoryName = productCategoryName;
        this.categoryDescription = productCategoryDescription;
        this.categoryDiscount = productCategoryDiscount;
        this.categoryImageUrl = productCategoryUrl;
    }

    /**
     * @return the idproductcategory
     */
    public String getProductCategoryName() {
        return categoryName;
    }

    /**
     * @param idproductcategory the idproductcategory to set
     */

    /**
     * @return the productDescription
     */
    public String getProductCategoryDescription() {
        return categoryDescription;
    }

    /**
     * @param productDescription the productDescription to set
     */

    /**
     * @return the productDiscount
     */
    public String getProductCategoryDiscount() {
        return categoryDiscount;
    }

    /**
     * @param productDiscount the productDiscount to set
     */

    /**
     * @return the productUrl
     */
    public String getProductCategoryImageUrl() {
        return categoryImageUrl;
    }

    /**
     * @param productUrl the productUrl to set
     */


}
