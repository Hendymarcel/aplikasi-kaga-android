package com.danma.kaga.model.entities;

public class Product {


    /**
     * The item short desc.
     */
    private String description = "";

    /**
     * The item detail.
     */
    private String longDescription = "";

    /**
     * The mrp.
     */
    private String mrp;

    /**
     * The discount.
     */
    private String discount;

    /**
     * The sell mrp.
     */
    private String salePrice;

    /**
     * The quantity.
     */
    private String orderQty;

    /**
     * The image url.
     */
    private String imageUrl = "";

    /**
     * The item name.
     */
    private String productName = "";

    private String productId = "";

    /**
     * @param itemName
     * @param itemShortDesc
     * @param itemDetail
     * @param MRP
     * @param discount
     * @param sellMRP
     * @param quantity
     * @param imageURL
     */
    public Product(String itemName, String itemShortDesc, String itemDetail,
                   String MRP, String discount, String sellMRP, String quantity,
                   String imageURL, String orderId) {
        this.productName = itemName;
        this.description = itemShortDesc;
        this.longDescription = itemDetail;
        this.mrp = MRP;
        this.discount = discount;
        this.salePrice = sellMRP;
        this.orderQty = quantity;
        this.imageUrl = imageURL;
        this.productId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public String getItemName() {
        return productName;
    }

    public String getItemShortDesc() {
        return description;
    }

    public String getItemDetail() {
        return longDescription;
    }

    public String getDiscount() {
        return discount + "%";
    }

    public String getSellMRP() {
        return salePrice;
    }

    public String getQuantity() {
        return orderQty;
    }

    public String getImageURL() {
        return imageUrl;
    }

}
