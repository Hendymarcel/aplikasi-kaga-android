package com.danma.kaga.util;

import android.content.Context;
import android.preference.PreferenceManager;

public class PreferenceHelper {

    public final static String SUBMIT_LOGS = "CrashLogs";
    // Handle Local Caching of data for responsiveness
    public static final String MY_CART_LIST_LOCAL = "MyCartItems";
    private static PreferenceHelper preferenceHelperInstance = new PreferenceHelper();

    private PreferenceHelper() {
    }

    public static PreferenceHelper getPrefernceHelperInstace() {

        return preferenceHelperInstance;
    }

    public void setBoolean(Context appContext, String key, Boolean value) {

        PreferenceManager.getDefaultSharedPreferences(appContext).edit()
                .putBoolean(key, value).apply();
    }

    // To retrieve values from shared preferences:

    public boolean getBoolean(Context appContext, String key,
                              Boolean defaultValue) {

        return PreferenceManager.getDefaultSharedPreferences(appContext)
                .getBoolean(key, defaultValue);
    }

}
